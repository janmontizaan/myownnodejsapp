const express = require('express')
const app = express()
const port = process.env.PORT || 3000

const fs = require('fs');

// var logger = require('tracer').colorConsole()

const logger = require('tracer')
    .colorConsole(
        {
            format: [
                "{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})", //default format
                {
                    error: "{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})\n}" // error format
                }
            ],
            dateformat: "HH:MM:ss.L",
            preprocess: function (data) {
                data.title = data.title.toUpperCase();
            },
            level: process.env.LOG_LEVEL || 'warn'
        });



console.log('Hello there')


logger.log('hello again');
logger.trace('hello', 'world');
logger.debug('hello %s', 'world', 123);
logger.info('hello %s %d', 'world', 123, { foo: 'bar' });
logger.warn('hello %s %d %j', 'world', 123, { foo: 'bar' });
logger.error('hello %s %d %j', 'world', 123, { foo: 'bar' }, [1, 2, 3, 4], Object);

console.log('hello %s', 'world', 123)

app.get('/', function(req, res){
    res.send('Hello from Heroku')
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))